﻿using System;
using System.IO;
namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] fileContent = File.ReadAllLines(@"files\test.txt");
            foreach(var content in fileContent)
            {
                Console.WriteLine(content);
            }
            Console.ReadLine();
        }
    }
}
